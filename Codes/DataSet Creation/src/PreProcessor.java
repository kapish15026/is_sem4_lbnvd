import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class PreProcessor {

	private String edgeFileName;
	private ArrayList<String> addedNodes;
	private HashSet<String>   visitedNodes;
	private ArrayList<String> addedServiceCenters;
	private HashMap<String,ArrayList<String>> edgesList;
	Queue<String> queue = new LinkedList<>();
	private String sourceVertex;
	private int requiredGraphSize;
	public static String SPLITBYCOMMA = ",";
	
	PreProcessor(String edgeFileName,String sourceVertex,int requiredGraphSize){
		
		this.edgeFileName = edgeFileName;
		this.sourceVertex = sourceVertex;
		this.requiredGraphSize = requiredGraphSize;
		this.addedNodes = new ArrayList<String>();
		this.addedServiceCenters = new ArrayList<String>();
		this.visitedNodes = new HashSet<String>();
		this.edgesList = new HashMap<String,ArrayList<String>>();
	}

	public void preProcessEdgeFile() throws IOException{
		BufferedReader bufferedReader = new BufferedReader(new FileReader(this.edgeFileName));
		String line = "";
		while ((line = bufferedReader.readLine()) != null) {
			//splitting the file with delimiter ','
			String[] details = line.split(SPLITBYCOMMA);
			System.out.println(line);
			if(edgesList.containsKey(details[0])){
				edgesList.get(details[0]).add(details[1]);
			}
			else{
				ArrayList<String> adjacentList = new ArrayList<String>();
				adjacentList.add(details[1]);
				edgesList.put(details[0], adjacentList);
			}
		}
		
		bufferedReader.close();
	}
	
	
	public void performBFS(){
		queue.add(sourceVertex);
		visitedNodes.add(sourceVertex);
		System.out.println("Size is "+this.requiredGraphSize);
		while (queue.size() != 0 && addedNodes.size() < this.requiredGraphSize){
			String dequeueNode = queue.poll();
			addedNodes.add(dequeueNode);
			Iterator<String> edgeIterator = edgesList.get(dequeueNode).iterator();
			while(edgeIterator.hasNext()){
				String adjacentNode = edgeIterator.next();
				if(!visitedNodes.contains(adjacentNode)){
					visitedNodes.add(adjacentNode);
					queue.add(adjacentNode);
				}
			}
		}
	}
	
	public void WriteToSubGraphEdgeFile(String subGraphEdgeFile) throws IOException{
		BufferedReader bufferedReader = new BufferedReader(new FileReader(this.edgeFileName));
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(subGraphEdgeFile));
		String line = "";
		while ((line = bufferedReader.readLine()) != null) {
			String[] details = line.split(SPLITBYCOMMA);
			if(addedNodes.contains(details[0]) && addedNodes.contains(details[1])){
				bufferedWriter.write(line);
				bufferedWriter.newLine();
			}
		}
		bufferedReader.close();
		bufferedWriter.close();
	}
	
	public void WriteToAllVerticesFile(String allVerticesFile) throws IOException{
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(allVerticesFile));
		for(int i=0;i<addedNodes.size();i++){
			bufferedWriter.write(addedNodes.get(i));
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
	}
	
	private int getRandomNo(int min,int max){
		Random rand = new Random();
		int randomNo = rand.nextInt((max-min)+1) + min;
		return randomNo;
	}
	public void WriteToServiceCenterFiles(String serviceCenterFiles,int perNodes,
			int penaltyLowerBound,int penaltyUpperBound,
			int capacityLowerBound,int capacityUpperBound) throws IOException{
		
		for(int i=0;i<this.requiredGraphSize;i += perNodes){
			addedServiceCenters.add(addedNodes.get(i));
		}
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(serviceCenterFiles));
		Iterator<String> scIterator = addedServiceCenters.iterator();
		while(scIterator.hasNext()){
			String sc = scIterator.next();
			int capacity = getRandomNo(capacityLowerBound,capacityUpperBound);
			int penalty  = getRandomNo(penaltyLowerBound,penaltyUpperBound);
			String details = "545641483,28.5978094,77.1808404,Secondary School,"+sc
					+",28.5973249,77.181114,"+capacity+","+penalty;
			bufferedWriter.write(details);
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
	}
	
	public String getEdgeFileName() {
		return edgeFileName;
	}

	public String getSourceVertex() {
		return sourceVertex;
	}

	public ArrayList<String> getAddedNodes() {
		return addedNodes;
	}

	public HashSet<String> getVisitedNodes() {
		return visitedNodes;
	}

	public ArrayList<String> getAddedServiceCenters() {
		return addedServiceCenters;
	}

	public HashMap<String, ArrayList<String>> getEdgesList() {
		return edgesList;
	}
	
	
	
}
