import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class DataSetCreator {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		PreProcessor p = new PreProcessor("with8kEdges.txt","838393152",7000);
		p.preProcessEdgeFile();
		HashMap<String,ArrayList<String>> edgesList = p.getEdgesList();
		Iterator<String> outgoingEdgesList = edgesList.keySet().iterator();
		while(outgoingEdgesList.hasNext()){
			String node = outgoingEdgesList.next();
			System.out.print(node+"-->");
			ArrayList<String> adjacentNodes = edgesList.get(node);
			for(int i=0;i<adjacentNodes.size();i++){
				System.out.print(adjacentNodes.get(i)+",");
			}
			System.out.println();
		} 
		
		p.performBFS();
		
		p.WriteToSubGraphEdgeFile("with7kEdges.txt");
		p.WriteToAllVerticesFile("allNew7k.txt");
		
		/*String serviceCenterFiles,int perNodes,
		int penaltyLowerBound,int penaltyUpperBound,
		int capacityLowerBound,int capacityUpperBound
		*/
		p.WriteToServiceCenterFiles("pureServiceVertices7k.txt",400,50,80,250,350);
		System.out.println("Done with work");
	}

}
