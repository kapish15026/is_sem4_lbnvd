package in.ac.iiitd.spatial;

public class PathNode {
	private String rejectedDemandNodeID;
	private String serviceCenterID;
	private int transferCost;
	private boolean isOverLoaded; 
	
	public PathNode(String rejectedDemandNodeID, String serviceCenterID, int transferCost,boolean isOverLoaded) {
		super();
		this.rejectedDemandNodeID = rejectedDemandNodeID;
		this.serviceCenterID = serviceCenterID;
		this.transferCost = transferCost;
		this.isOverLoaded = isOverLoaded;
	}

	public String getRejectedDemandNodeID() {
		return rejectedDemandNodeID;
	}
	
	public String getServiceCenterID() {
		return serviceCenterID;
	}
	
	public int getTransferCost() {
		return transferCost;
	}

	public boolean isOverLoaded() {
		return isOverLoaded;
	}

	public void setTransferCost(int transferCost) {
		this.transferCost = transferCost;
	}

	public void setOverLoaded(boolean isOverLoaded) {
		this.isOverLoaded = isOverLoaded;
	}
	
	
}
