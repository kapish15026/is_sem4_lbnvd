package in.ac.iiitd.spatial;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
/*
 * This class loads service centers, demand vertices and distance matrix from a file.
 */

public class PreProcessor {
	
	public static String SPLITBYCOMMA = ",";
	public static int    CONSTZERO    = 0;
	public static int    CONSTONE     = 1;
	public static int    CONSTTWO     = 2;
	public static int    CONSTFOUR    = 4;
	public static int    CONSTSEVEN   = 7;
	public static int    CONSTEIGHT   = 8;
	public static String CONSTSPACE   = " ";
	public static int    CONSTGREYCOL = 8224125;
	public static HashMap<String, Vertex> allVertex    = new HashMap<String, Vertex>();
	public static HashMap<String, Vertex> demandVertex = new HashMap<String, Vertex>();
	public static HashMap<String, ServiceCenter> SC    = new HashMap<String, ServiceCenter>();
	public static ArrayList<String> availableSCIDs     = new ArrayList<String>();
	//store global pool of service centers in HashMap <ServiceCenterIDs, capacity-penalty>
	public static HashMap<String,String> serviceCenterIDs     = new HashMap<String,String>();
	
	//Stores distances of demand node to various service centers with id as key. 
	public static HashMap<String,ArrayList<Integer>> distanceMatrix = new HashMap<String,ArrayList<Integer>>();
	
	public static HashMap<Integer,String> indexToSCIDMapping = new HashMap<Integer,String>();
	
	//this routine takes path of file containing service center details as argument and load in the Hash map
	public static void loadAllUniqueServiceCenters(String filePath) throws IOException{
		//file Path
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
		String line = "";
		while ((line = bufferedReader.readLine()) != null) {
			//splitting the file with delimiter ',' and fourth position is of Id
			String[] details = line.split(SPLITBYCOMMA);
			if(!serviceCenterIDs.containsKey(details[CONSTFOUR])){
				if(!details[CONSTFOUR].equals(CONSTSPACE)){
					//System.out.println("Adding SCPool "+details[CONSTFOUR]+SPLITBYCOMMA+details[CONSTSEVEN]+SPLITBYCOMMA+details[CONSTEIGHT]);
					//4th Pos -> Service Center ID, 7th Pos -> Capacity,8th Pos -> Penalty
					serviceCenterIDs.put(details[CONSTFOUR],details[CONSTSEVEN]+SPLITBYCOMMA+details[CONSTEIGHT]);
				}
			}
		}
		//System.out.println("Total No of service center added in GlobalPool "+serviceCenterIDs.size());
		bufferedReader.close();
	}
	
	//this function load all the vertex id along with vertex object(id and color) in HashMap
	public static void loadAllVertices(String filePath) throws IOException{
		BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
		String line = CONSTSPACE;
		int index = CONSTZERO;
		while((line = bufferedReader.readLine()) != null){
			String[] details = line.split(SPLITBYCOMMA);
			Vertex vertex = new Vertex(details[CONSTZERO],index,CONSTGREYCOL); // grey color
			allVertex.put(details[0], vertex);
			//System.out.println("Adding Vertex "+details[0]+" Its Index "+index);
			if(!serviceCenterIDs.containsKey(details[CONSTZERO])){
				demandVertex.put(details[CONSTZERO],vertex);
				System.out.println("It is demand Node "+vertex.getLabel()+" at Pos: "+index);
			}
			else{
				availableSCIDs.add(details[CONSTZERO]);
				//System.out.println("It is Service Center "+details[CONSTZERO]+" at Pos: "+index);
			}
			index++;
			
		}
		/*System.out.println("Total no of all vertices are "+allVertex.size());
		System.out.println("Total no of demand vertices are "+demandVertex.size());
		System.out.println("Total no of service centers are "+availableSCIDs.size()); */
		bufferedReader.close();
	}
	
	
	
	//load Service Center Vertices argument ns-> no of service centers to be loaded
	public static void loadSC(int ns) {
		int totalServiceCenters = availableSCIDs.size();
		if(ns > totalServiceCenters){ //no of service centers to be loaded are more than no of service center available
			System.out.println("Not able to load service center. Service centers are few");
		}
		
		else{
			
			for(int i=0;i<ns;i++){
				Vertex scVertex = allVertex.get(availableSCIDs.get(i));
				String[] scMetrices = serviceCenterIDs.get(availableSCIDs.get(i)).split(SPLITBYCOMMA);
				int capacity = Integer.parseInt(scMetrices[CONSTZERO]);
				int penalty  = Integer.parseInt(scMetrices[CONSTONE]);
				
				ServiceCenter serviceCenter = new ServiceCenter(availableSCIDs.get(i),
						scVertex.getVertexIndex(),Constants.color[i], capacity, penalty);
				SC.put(availableSCIDs.get(i), serviceCenter);
				/*System.out.println("SC in Vornoi "+scVertex.getLabel()+" at Pos: "+scVertex.getVertexIndex());
				System.out.println("Color, Capacity and Penalty added "+
				serviceCenter.getColor()+SPLITBYCOMMA+serviceCenter.getCurrCapacity()+
				SPLITBYCOMMA+serviceCenter.getPenalty());*/
			
			}
		}
		//System.out.println("Total No of Service Center Loaded in Vornoi "+SC.size());
	}
	
	/* Load everything in CCNVD */
	public static void loadCCNVD(String filePath) throws IOException {
		int edgesCount = 0;
		HashMap<String, List<GraphNode>> graph = new HashMap<>();
		BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
		String line = CONSTSPACE;
		while ((line = bufferedReader.readLine()) != null) {
			String[] details  = line.split(SPLITBYCOMMA);
			if (allVertex.containsKey(details[CONSTZERO]) //check if both of the vertices are present in the allVertex HashMap
					&& allVertex.containsKey(details[CONSTONE])) {
				edgesCount++;
				List<GraphNode> existingGraphNodeList = graph.get(details[CONSTZERO]);
				if (existingGraphNodeList != null) {
					GraphNode graphNode = new GraphNode(details[CONSTONE],Integer.parseInt(details[CONSTTWO]));
					existingGraphNodeList.add(graphNode);
					System.out.println(details[0]+SPLITBYCOMMA+details[CONSTONE]
							+SPLITBYCOMMA+Integer.parseInt(details[CONSTTWO])+" Existing node");
				} 
				else {
					List<GraphNode> newGraphNodeList = new ArrayList<>();
					GraphNode graphNode = new GraphNode(details[CONSTONE],Integer.parseInt(details[CONSTTWO]));
					newGraphNodeList.add(graphNode);
					graph.put(details[CONSTZERO], newGraphNodeList);
					System.out.println(details[0]+SPLITBYCOMMA+details[CONSTONE]
							+SPLITBYCOMMA+Integer.parseInt(details[CONSTTWO])+" New node");
				}
				
			}
		}
		bufferedReader.close();
		Loral.CCNVD.setGraph(graph);
		List<ServiceCenter> list = new ArrayList<ServiceCenter>(SC.values());
		Loral.CCNVD.setSC(list);
		Loral.CCNVD.setDemandVertices(demandVertex);
		System.out.println("Graph size " + graph.size());
		System.out.println("Edges Count " + edgesCount);
	}
	
	
	public static void LoadDistanceMatrixAndBuildMinHeap(String filePath) throws IOException{
		
		//first load the entire distance matrix (n*n) n= nsg+nd (nsg -> global service centers) in the local variable tempDistanceMatrix
		int totalVertex = allVertex.size();
		int[][] tempDistanceMatrix = new int[totalVertex][];
		BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
		String line = CONSTSPACE;
		int i = CONSTZERO; //counter for row
		while((line = bufferedReader.readLine()) != null){
			String[] distanceRow = line.split(SPLITBYCOMMA);
			tempDistanceMatrix[i] = new int[totalVertex];
			for(int j=0;j<totalVertex;j++){
				
				tempDistanceMatrix[i][j] = Integer.parseInt(distanceRow[j].replaceAll("\\s",""));
			//	System.out.print(tempDistanceMatrix[i][j]+",");
				
			}
		//	System.out.println();
	
			i++;
		}
		bufferedReader.close();
		
		//loading only required part of entire distance matrix
		Iterator<String> demandVertexIterator  = demandVertex.keySet().iterator();
		int count = 0;
		while(demandVertexIterator.hasNext()){
			Vertex demandNode = demandVertex.get(demandVertexIterator.next());
		//	System.out.println("Distances for DN "+demandNode.getLabel());
			ArrayList<Integer> distances = new ArrayList<Integer>();
			Iterator<String> serviceCenterIterator = SC.keySet().iterator();
			int minDistance = Integer.MAX_VALUE;
			String minServiceCenterID = "";
			while(serviceCenterIterator.hasNext()){
				ServiceCenter serviceCenterNode = SC.get(serviceCenterIterator.next());
				int distance = tempDistanceMatrix[demandNode.getVertexIndex()][serviceCenterNode.getScIndex()];
				distances.add(distance);
				
				if(count < SC.size()){
					indexToSCIDMapping.put(count, serviceCenterNode.getServiceCenterId());
					count++;
				}
				
				if(minDistance > distance){
					minDistance = distance;
					minServiceCenterID = serviceCenterNode.getServiceCenterId();
				}
				
			}
			MinHeapNode minHeapNode = new MinHeapNode(minDistance,demandNode.getLabel(),minServiceCenterID);
			Loral.minheap.add(minHeapNode);
			distanceMatrix.put(demandNode.getLabel(),distances);
		}
		//Test.PrintDistanceMatrix();
		//assigning Index of service center according to distance matrix taken
		Iterator<Integer> mappingSCIterator = indexToSCIDMapping.keySet().iterator();
		while(mappingSCIterator.hasNext()){
			int index = mappingSCIterator.next();
			ServiceCenter serviceCenter = SC.get(indexToSCIDMapping.get(index));
		/*	System.out.println("Index : "+index+CONSTSPACE+serviceCenter.getServiceCenterId()+" Old Pos "+
			serviceCenter.getScIndex()); */
			serviceCenter.setScIndex(index);
			//System.out.println(serviceCenter.getScIndex());
		}
		
	}
}
