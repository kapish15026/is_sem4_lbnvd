package in.ac.iiitd.spatial;

import java.util.HashMap;
import java.util.HashSet;


/**
 * Description about the class
 */

public class ServiceCenter {
    
	private String serviceCenterId;
    private Integer color;
    private int scIndex;
    private int maxCapacity;
    private int currCapacity;
    private int penalty;
    HashMap<String, GraphNode> boundary;
    HashSet<String> allocations;
   
	public ServiceCenter(String serviceCenterId, int scIndex, Integer color, int maxCapacity, int penalty){
        this.serviceCenterId = serviceCenterId;
        this.scIndex         = scIndex;
        this.color           = color;
        this.maxCapacity     = maxCapacity;
        this.currCapacity    = maxCapacity;
        this.penalty         = penalty;
        this.boundary        = new HashMap<>();
        this.allocations     = new HashSet<>();
    }
	
	 public HashMap<String, GraphNode> getBoundary() {
		return boundary;
	 }
	 
	 public void addNewAllocation(String scID){
		 allocations.add(scID);
	 }
	 
	public HashSet<String> getAllocations() {
		return allocations;
	}

	public void setBoundary(HashMap<String, GraphNode> boundary) {
		this.boundary = boundary;
	}

    public int getScIndex() {
		return scIndex;
	}
    public void setScIndex(int scIndex) {
		this.scIndex = scIndex;
	}

    public String getServiceCenterId() {
        return serviceCenterId;
    }

    public Integer getColor() {
        return color;
    }

    
    public int getMaxCapacity() {
        return maxCapacity;
    }

    
    public int getCurrCapacity() {
        return currCapacity;
    }

    
    public void setCurrCapacity(int currCapacity) {
		this.currCapacity = currCapacity;
		
		if(currCapacity == 0){
			
		}
	}

	public int getPenalty() {
        return penalty;
    }

    
}
