package in.ac.iiitd.spatial;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.TreeSet;

public class Loral {
	
	private int objectiveFunctionCost = 0;
	private HashMap<String,String> assignments = new HashMap<String,String>();
	private int maxPathLengthSize;
	private int k;
	
	public static int CONSTGREYCOL = 8224125;
	public static PriorityQueue<MinHeapNode> minheap=new PriorityQueue<MinHeapNode>(1,new Comparator<MinHeapNode>() {
        @Override
        public int compare(MinHeapNode o1, MinHeapNode o2) {
        	if(o1.getDistance() == o2.getDistance()){
        		return o1.getDemandNodeID().compareTo(o2.getDemandNodeID());
        	}
        	else{
        		return o1.getDistance()-o2.getDistance();
        	}
        	
        }
    });
	public static CCNVoronoiD CCNVD = new CCNVoronoiD();
	

	public Loral(int maxPathLengthSize, int k) {
		this.maxPathLengthSize = maxPathLengthSize;
		this.k = k;
	}


			
	//main Algorithm function
	public void PerformLoral(){
		HashMap<String, Vertex> demandNodeList = CCNVD.getDemandVertices();
		//Test.printSCStatus();
		while(true){
			if(minheap.size() == 0){
				//no demand vertex left uncolored/assigned
				break;
			}

			MinHeapNode minHeapNode = minheap.element();
			
			//Demand Node ID
			String demandNodeID = minHeapNode.getDemandNodeID();
			//Service Center
			ServiceCenter serviceCenter = PreProcessor.SC.get(minHeapNode.getServiceCenterID());
			//capacity of above service Center 
			int currCapacity = serviceCenter.getCurrCapacity();
			System.out.println(demandNodeID+"  "+serviceCenter.getServiceCenterId()+"   "+minHeapNode.getDistance());
			if(currCapacity > 0){
				System.out.println("Normal Assignment will take place");
				//normal assignment of demand node to service center

				/*
				 * 1-> color the demandNode of the same color as of Service center 
				 * 2-> reduce the current capacity of service center by 1
				 * 3-> add distance cost to global objective function
				 * 4-> update the boundary nodes 
				*/
				
				demandNodeList.get(demandNodeID).setColor(serviceCenter.getColor());
				serviceCenter.setCurrCapacity(currCapacity-1);
				objectiveFunctionCost += minHeapNode.getDistance();
				assignments.put(demandNodeID, serviceCenter.getServiceCenterId());
				CheckUpdateAndInsertBoundaryNodes(minHeapNode);
				serviceCenter.addNewAllocation(demandNodeID);
			}
			else{
				System.out.println("Overloading or Transfer will happen");
				//if there is no capacity left then min of best k paths or overloading will happen
                //calculating the cost of overloading
				
				int penaltyCost = serviceCenter.getPenalty();
				int overloadingCost = minHeapNode.getDistance() + penaltyCost;
				
				//List of K Boundary PathNodes that can be rejected by the service center 
				TreeSet<PathNode> KBestPathNode = BestKBoundaryNodes(serviceCenter,this.k);
				Path minPath = null;
				Iterator<PathNode> pathNodeIterator = KBestPathNode.iterator();
				int totalChoices = KBestPathNode.size();
				System.out.println("Count of BestPathNodes to be enumerated "+totalChoices);
				while(pathNodeIterator.hasNext()){
					//explore all the paths and find the path which for which objective function cost is minimum.
					PathNode pathNodeToBeExplored = pathNodeIterator.next();
					//t ->maximum path length is sent as second parameter
					Path path = explorePath(pathNodeToBeExplored,this.maxPathLengthSize,serviceCenter.getServiceCenterId());
					System.out.println("Path Size is "+path.getPath().size()+" Path cost "+path.getPathCost());
					if(minPath == null || path.getPathCost() < minPath.getPathCost()){
						minPath = path;
					}
				}
				int transferCost = 0;
				
				//this if is only put to avoid null pointer exception if minPath is null
				if(minPath != null)
				{
					System.out.println("Path Cost "+minPath.getPathCost());
					transferCost = (minHeapNode.getDistance() + minPath.getPathCost());
				}
				else{
					transferCost = Integer.MAX_VALUE;
				}

				//checking if overloading is better or path found has length 0. This will happen when there will be no SC
				System.out.println("Total Choices "+totalChoices);
				System.out.println("Transferring-Overloading Cost "+transferCost+" "+overloadingCost);
				if((totalChoices == 0) || (transferCost > overloadingCost)){
					System.out.println("Overloading is preferred");
					//do overloading
					//setting the color of Demand Node to that of Service Center
					demandNodeList.get(demandNodeID).setColor(serviceCenter.getColor());
					//reduce the current capacity of service center by 1
					serviceCenter.setCurrCapacity(currCapacity-1);
					//add distance cost to global objective function
					objectiveFunctionCost += overloadingCost;
					assignments.put(demandNodeID, serviceCenter.getServiceCenterId());
					
					//update the boundary nodes
					CheckUpdateAndInsertBoundaryNodes(minHeapNode);
					serviceCenter.addNewAllocation(demandNodeID);
					
				}
				else{ 
					//transferring will take place
					//do swaps according to path
					System.out.println("Local re-adjustment will take place");
					ArrayList<PathNode> minPathNodeList = minPath.getPath();
					//taking this variable to fetch the Boundaries and checking if RejectedDemandNode is there
					String VbbsSCID = serviceCenter.getServiceCenterId();
					System.out.println("Path length is "+minPathNodeList.size());
					for(int i=0;i<minPath.getPath().size();i++){
						PathNode pathNode = minPathNodeList.get(i);
						ServiceCenter r = PreProcessor.SC.get(pathNode.getServiceCenterID());
						String rejectedDemandNodeID = pathNode.getRejectedDemandNodeID();
						System.out.println("Rejected Demand Node ID "+rejectedDemandNodeID+" SCID: "+r.getServiceCenterId());
						System.out.println(VbbsSCID+" -> "+rejectedDemandNodeID+" -> "+r.getServiceCenterId());
						//change color to accepting Service Center Node
						demandNodeList.get(rejectedDemandNodeID).setColor(r.getColor());

						//update the boundary nodes
						int distanceOfDemandNodeToR = PreProcessor.distanceMatrix.
								get(pathNode.getRejectedDemandNodeID()).get(r.getScIndex());
						MinHeapNode heapNode = new MinHeapNode(distanceOfDemandNodeToR,
								rejectedDemandNodeID,r.getServiceCenterId());
						CheckUpdateAndInsertBoundaryNodes(heapNode);

						//removing demand node from boundary if its present in Previous SC
						HashMap<String,GraphNode> VbbsSCBoundary = PreProcessor.SC.get(VbbsSCID).getBoundary();
						if(VbbsSCBoundary.containsKey(rejectedDemandNodeID)){
							VbbsSCBoundary.remove(rejectedDemandNodeID);
						}
						//remove rejected node from previous allocations
						HashSet<String> allocationsForVbbs = PreProcessor.SC.get(VbbsSCID).getAllocations();
						if(allocationsForVbbs.contains(rejectedDemandNodeID)){
							//always true
							allocationsForVbbs.remove(rejectedDemandNodeID);
						}
						
						VbbsSCID = r.getServiceCenterId();
						
						//update allocations of accepting service center i.e. r
						r.addNewAllocation(rejectedDemandNodeID);
						
						//putting every assignment in result HashMap
						assignments.put(rejectedDemandNodeID, r.getServiceCenterId());
					}
					//Capacity of last SC in path must be reduced by 1
					ServiceCenter lastServiceCenter = PreProcessor.SC.get(minPathNodeList.get(minPathNodeList.size()-1).getServiceCenterID());
					lastServiceCenter.setCurrCapacity(lastServiceCenter.getCurrCapacity()-1);

					//assigning demand Node extracted from min heap to its service center

					//setting the color of Demand Node to that of Service Center
					demandNodeList.get(demandNodeID).setColor(serviceCenter.getColor());

					//update the boundary nodes
					CheckUpdateAndInsertBoundaryNodes(minHeapNode);
					
					serviceCenter.addNewAllocation(demandNodeID);
					
					objectiveFunctionCost += minPath.getPathCost() + minHeapNode.getDistance();
					assignments.put(demandNodeID, serviceCenter.getServiceCenterId());
					
				}


			}
			minheap.remove();
			//printAllocations();
			System.out.println("Objective Cost "+this.objectiveFunctionCost);
		//	System.out.println("----------------pass-------------------------");
			//Test.printSCStatus();
		//	Test.printSC(serviceCenter.getServiceCenterId());
			
			Test.printSCStatus();
			System.out.println("-------------------- Next Assignment --------------------");
		}
	}
	void printAllocations(){
		Iterator<String> iter = assignments.keySet().iterator();
		
		while(iter.hasNext()){
			String id = iter.next();
			System.out.println(id+"  "+assignments.get(id));
		}
		
	}
	//t -> maximum path length
	private Path explorePath(PathNode pathNodeToBeExplored,int t,String scID) {
		// TODO Auto-generated method stub
		HashSet<String> visitedServiceCenters  = new HashSet<String>();
		Path path = new Path();
		int i = 1;
		PathNode pathNode = pathNodeToBeExplored;
		
		
		ServiceCenter firstSC = PreProcessor.SC.get(scID);
		
		visitedServiceCenters.add(scID);
		System.out.println("Path exploration starts");
		while(true){
			System.out.println("Transferring DemandNode "+pathNode.getRejectedDemandNodeID()+
					" to Service Center "+pathNode.getServiceCenterID());
			if(i >= t){
				System.out.println("Counter Value to check for path length "+i);
				System.out.println("Path length cannot be more than "+t);
				path.addPathNode(pathNode);
				path.setPathCost(path.getPathCost() + pathNode.getTransferCost());
				break;
			}
			else{
				if(!pathNode.isOverLoaded()){
					System.out.println("Service center is not overloaded. Reached Dead End");
					path.addPathNode(pathNode);
					path.setPathCost(path.getPathCost() + pathNode.getTransferCost());
					break;
				}
				else{
					//transfer phase 
					//move one of boundary to other SC (best one)
					String VbbsID = pathNode.getServiceCenterID();
					ServiceCenter Vbbs = PreProcessor.SC.get(VbbsID);
					TreeSet<PathNode> KBestPathNode = BestKBoundaryNodes(Vbbs,1);
					
					if(KBestPathNode.size() == 0)
					{
						System.out.println("Nothing best found");
						path.addPathNode(pathNode);
						path.setPathCost(path.getPathCost() + pathNode.getTransferCost());
						break;
					}
					else{
						PathNode newPathNode = KBestPathNode.first();
						System.out.println(newPathNode.getServiceCenterID()+"  "+pathNode.getServiceCenterID());
						if(visitedServiceCenters.contains(newPathNode.getServiceCenterID())){
							System.out.println("Cycle is forming");
							path.addPathNode(pathNode);
							path.setPathCost(path.getPathCost() + pathNode.getTransferCost());
							break;
						}
						else{
							//if not forming the cycle then transfer will happen
							System.out.println("Transferring will take place");
							
							
							
							//overloading is better than transfer
							if((newPathNode.getTransferCost() > Vbbs.getPenalty())){
								path.addPathNode(pathNode);
								path.setPathCost(path.getPathCost() + pathNode.getTransferCost());
								break;
							}
							else if(firstSC.getPenalty() <= (path.getPathCost() + pathNode.getTransferCost() - Vbbs.getPenalty() + newPathNode.getTransferCost())){
								path.addPathNode(pathNode);
								path.setPathCost(path.getPathCost() + pathNode.getTransferCost());
								break;
							}
							else{
								//transfer will take place
								pathNode.setTransferCost(pathNode.getTransferCost() - Vbbs.getPenalty());
								pathNode.setOverLoaded(false);
								path.addPathNode(pathNode);
								path.setPathCost(path.getPathCost() + pathNode.getTransferCost());
								visitedServiceCenters.add(pathNode.getServiceCenterID());
								pathNode = newPathNode;
							}
							
						}
				
					} 		
					
				}
			}
			
			i++;
		}
		return path;
	}

	private TreeSet<PathNode> BestKBoundaryNodes(ServiceCenter serviceCenter,int k){
		TreeSet<PathNode> kBestPathNode = new TreeSet<PathNode>(new Comparator<PathNode>() {
	        @Override
	        public int compare(PathNode o1, PathNode o2) {
	        	if(o1.getTransferCost() == o2.getTransferCost()){
	        		return 1;
	        	}
	        	else{
	        		return o1.getTransferCost()-o2.getTransferCost();
	        	}
	            
	        }
	    });
		 
		int serviceCenterIndex = serviceCenter.getScIndex(); //Index of service center in Distance Matrix
		//fetching all the boundary which is collection of <DemandNodeID,GraphNode(demandNodeID,weight)>
		Iterator<String> boundaryIterator = serviceCenter.getBoundary().keySet().iterator();
		
		while(boundaryIterator.hasNext()){
			String demandNodeID = boundaryIterator.next();
			System.out.println("Finding minimum transfer cost for "+demandNodeID);
			//fetching the distance arraylist for the corresponding demand Node from Distance Matrix
			ArrayList<Integer> distances = PreProcessor.distanceMatrix.get(demandNodeID);
			String bestAcceptingServiceCenter = "";
			int minCost = Integer.MAX_VALUE;
			boolean isMinSCOverLoaded = false;
			boolean isSCOverLoaded = false;
			for(int i=0;i<distances.size();i++){
				//when SC index not equal to index from where rejection is going to take place
				if(i != serviceCenterIndex){
					int currCapacity = PreProcessor.SC.get(PreProcessor.indexToSCIDMapping.get(i)).getCurrCapacity();
					int totalTransferCost = 0;
					if(currCapacity > 0){
						isSCOverLoaded = false;
						totalTransferCost = distances.get(i) - distances.get(serviceCenterIndex);
					}
					else{
						isSCOverLoaded = true;
						int penalty = PreProcessor.SC.get(PreProcessor.indexToSCIDMapping.get(i)).getPenalty();
						totalTransferCost = penalty + distances.get(i) - distances.get(serviceCenterIndex);
					}
					
					if(totalTransferCost < minCost){
						minCost = totalTransferCost;
						isMinSCOverLoaded = isSCOverLoaded;
						bestAcceptingServiceCenter = PreProcessor.indexToSCIDMapping.get(i);
					}
				}
			}
			System.out.println("Minimum Transfer Cost evaluated is "+minCost+" by transferring to "
					+bestAcceptingServiceCenter+" from "+serviceCenter.getServiceCenterId());
			PathNode pathNode = new PathNode(demandNodeID,bestAcceptingServiceCenter,minCost,isMinSCOverLoaded);
			kBestPathNode.add(pathNode);
			//System.out.println("Size is "+kBestPathNode.size());
			
		}
		System.out.println("Size is "+kBestPathNode.size()+"  "+k);
		if(k >= kBestPathNode.size()){
			System.out.println("k value is "+k+"  KBestPathNode Size is "+kBestPathNode.size());
			return kBestPathNode;
		}
		else{
			System.out.println("Counter value in KBESTBOUND  "+kBestPathNode.size());
			int counter = kBestPathNode.size() -k;
			for(int i=0;i<(counter);i++){
				
				kBestPathNode.pollLast();
				
			}
			System.out.println("Total KBestLength "+kBestPathNode.size());
			return kBestPathNode;
		}
			
		
	}
	private void CheckUpdateAndInsertBoundaryNodes(MinHeapNode minHeapNode) {
		
		//update exisiting boundary nodes of service center
		ServiceCenter serviceCenter = PreProcessor.SC.get(minHeapNode.getServiceCenterID());
		Iterator<String> allocatedIterator = serviceCenter.getAllocations().iterator();
		
		while(allocatedIterator.hasNext()){
			String demandNodeID = allocatedIterator.next();
			
			if(!checkBoundary(demandNodeID,serviceCenter.getServiceCenterId())){
				serviceCenter.getBoundary().remove(demandNodeID);
				System.out.println("Demand Node : "+demandNodeID+" is no more boundary");
			}
			else{
				if(!serviceCenter.getBoundary().containsKey(demandNodeID)){
					//as we are not using distance in graphnode so distance is taken as 0 by default
					GraphNode graphNode = new GraphNode(demandNodeID,0);
					serviceCenter.getBoundary().put(demandNodeID, graphNode);
				}
				System.out.println("Demand Node : "+demandNodeID+" is boundary");
			}
		}
		// TODO Auto-generated method stub
		if(checkBoundary(minHeapNode.getDemandNodeID(),minHeapNode.getServiceCenterID())){
			System.out.println("Assigning Boundary Node "+minHeapNode.getDemandNodeID()+" to SC "
				+minHeapNode.getServiceCenterID());
			//insert the demand node in boundary list of service center
			GraphNode graphNode = new GraphNode(minHeapNode.getDemandNodeID(),minHeapNode.getDistance());
			HashMap<String,GraphNode> existingBoundary = serviceCenter.getBoundary();
			existingBoundary.put(minHeapNode.getDemandNodeID(),graphNode);
			
		}
	}
	
	private boolean checkBoundary(String demandNodeID,String serviceCenterID){
		boolean isBoundaryNode = false;
		int color = CCNVD.getDemandVertices().get(demandNodeID).getColor();
		//fetch all the adjacent Node list from the CCNVD
		List<GraphNode> graphNodeList = CCNVD.getGraph().get(demandNodeID);
		System.out.println("Checking for Demand Node: "+demandNodeID+"-"+color);
		for(int i=0;i<graphNodeList.size();i++){
			String adjacentNodeID = graphNodeList.get(i).getV();
			if(CCNVD.getDemandVertices().containsKey(adjacentNodeID)){
				int adjacentNodeColor = CCNVD.getDemandVertices().get(adjacentNodeID).getColor();
				System.out.print("Adjacent Demand Node: "+adjacentNodeID+"-"+adjacentNodeColor);
				if(color != adjacentNodeColor || adjacentNodeColor == CONSTGREYCOL){
					System.out.println(" has gray or different color");
					isBoundaryNode = true;
					break;
				}
				else{
					System.out.println(" has same color");
				}
			}
			else{
				//adjacent node is service center 
				//adjacent node is not one SC to which demand node is assigned
				if(!adjacentNodeID.equals(serviceCenterID)){
					System.out.println("Adjacent Service center: "+adjacentNodeID);
					isBoundaryNode = true;
					break;
				}
			}
		}
		return isBoundaryNode;
	}
	
	public int getMaxPathLengthSize() {
		return maxPathLengthSize;
	}

	public int getK() {
		return k;
	}


	public int getObjectiveFunctionCost() {
		return objectiveFunctionCost;
	}

	public HashMap<String, String> getAssignments() {
		return assignments;
	}
	
	

	
}
