package in.ac.iiitd.spatial;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ankita_mehta on 8/6/16.
 */

/**
 * Description about the class
 */

public class CCNVoronoiD {
    HashMap<String, List<GraphNode>> graph;
    List<ServiceCenter> serviceCenters;
    HashMap<String,Vertex> demandVertices;

    
    
    
    public HashMap<String, Vertex> getDemandVertices() {
		return demandVertices;
	}

	public void setDemandVertices(HashMap<String, Vertex> demandVertices) {
		this.demandVertices = demandVertices;
	}

	public HashMap<String, List<GraphNode>> getGraph() {
        return graph;
    }

    public void setGraph(HashMap<String, List<GraphNode>> graph) {
        this.graph = graph;
    }

    public List<ServiceCenter> getSC() {
        return serviceCenters;
    }

    public void setSC(List<ServiceCenter> serviceCenters) {
        this.serviceCenters = serviceCenters;
    }
}
