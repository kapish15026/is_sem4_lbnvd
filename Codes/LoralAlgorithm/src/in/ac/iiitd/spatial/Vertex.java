package in.ac.iiitd.spatial;


/**
 * Created by ankita_mehta on 8/5/16.
 */

/**
 * Description
 */

public class Vertex {

    private String  label;
    private Integer color;
    private int     vertexIndex;
    
    public String getLabel() {
		return label;
	}

	public int getVertexIndex() {
		return vertexIndex;
	}

	public Integer getColor() {
		return color;
	}

	public void setColor(Integer color) {
		this.color = color;
	}

	public Vertex() {
	super();
    }

    public Vertex(String label, int vertexIndex, Integer color) {
	super();
	this.label = label;
	this.color = color;
	this.vertexIndex = vertexIndex;
    }

}
