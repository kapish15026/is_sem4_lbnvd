package in.ac.iiitd.spatial;


/**
 * Created by ankita_mehta on 8/8/16.
 */

/**
 * Description
 */

public class GraphNode {
    //Vertex v;
    String adjacentNode;
    int weight;

    public GraphNode(String adjacentNode, int weight) {
	super();
	this.adjacentNode = adjacentNode;
	this.weight = weight;
    }

    public GraphNode() {
	super();
    }

    public String getV() {
	return adjacentNode;
    }
   // ArrayList<GraphNode> adjacencyList;
}
