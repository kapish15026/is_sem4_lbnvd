package in.ac.iiitd.spatial;

import java.util.ArrayList;

public class Path {
	
	private ArrayList<PathNode> path;
	private int pathCost;
	public Path() {
		super();
		this.path = new ArrayList<PathNode>();
		this.pathCost = 0;
	}
	
	public int getPathCost() {
		return pathCost;
	}
	
	public void setPathCost(int pathCost) {
		this.pathCost = pathCost;
	}
	
	public ArrayList<PathNode> getPath() {
		return path;
	}
	
	public void addPathNode(PathNode pathNode){
		this.path.add(pathNode);
	}
	
	
	
}
