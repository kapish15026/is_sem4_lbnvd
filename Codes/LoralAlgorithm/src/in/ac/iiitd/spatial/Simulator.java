package in.ac.iiitd.spatial;

import java.io.IOException;
import java.util.Iterator;

public class Simulator {

	public static void main(String[] args) throws IOException  {
		// TODO Auto-generated method stub
		String fileSuffix = args[0];
		int scCount = Integer.parseInt(args[1]);
		int minPathLengthSize = Integer.parseInt(args[2]);
		int paths = Integer.parseInt(args[3]);
		PreProcessor.loadAllUniqueServiceCenters("pureServiceVertices"+fileSuffix+".txt"); //pureServiceCenter
		PreProcessor.loadAllVertices("allNew"+fileSuffix+".txt");
		PreProcessor.loadSC(scCount);
		PreProcessor.loadCCNVD("with"+fileSuffix+"Edges.txt");
		PreProcessor.LoadDistanceMatrixAndBuildMinHeap(fileSuffix+"distance.txt");
		
		//Test.PrintDistanceMatrix();
		//passing two paramaters in constructor minpath line size and k-> total paths to be enumerated
		Loral loral = new Loral(minPathLengthSize,paths);
	//	System.out.println("-------------------Started----------------------------");
		long startTime = System.nanoTime();
		loral.PerformLoral();
		long endTime   = System.nanoTime();
		long duration = (endTime - startTime);
		
	//	System.out.println("------------------Finished------------------------------");
		
		//Test.printSCStatus();
		Iterator<String> iterator = loral.getAssignments().keySet().iterator();
		while(iterator.hasNext()){
			String demandNodeID = iterator.next();
			String scID = loral.getAssignments().get(demandNodeID);
			System.out.println(demandNodeID+";"+scID); 
		} 
		System.out.println(loral.getObjectiveFunctionCost());
		System.out.println(duration/1000000000);   //in seconds 		
	} 

}
