package in.ac.iiitd.spatial;

public class Constants {
	
	public static Integer[] color = { 414534,
			829068,
			1243602,
			1658136,
			2072670,
			2487204,
			2901738,
			3316272,
			3730806,
			4145340,
			4559874,
			4974408,
			5388942,
			5803476,
			6218010,
			6632544,
			7047078,
			7461612,
			7876146,
			8290680,
			8705214,
			9119748,
			9534282,
			9948816,
			10363350,
			10777884,
			11192418,
			11606952,
			12021486,
			12436020,
			12850554,
			13265088,
			13679622,
			14094156,
			14508690,
			14923224,
			15337758,
			15752292,
			16166826,
			16581360
		// 8224125, //grey
		};
		public static Integer[] capacity = { 3, // red
			2, // yellow
			2, // cyan
			4, // pink
			1, // blue
			20, // grey
			200, // purple
			220, // red
			500, // yellow
			600, // cyan
			10, // pink
			100, // blue
			550, // grey
			121, // purple
			111, // red
			827, // yellow
			261, // cyan
			920, // pink
			120, // blue
			20, // grey
			1000, // purple
			137, // red
			2, // yellow
			219, // cyan
			163, // pink
			218, // blue
			1000, // grey
			20, // purple
			150, // red
			60, // yellow
			1260, // cyan
			203, // pink
			224, // blue
			225, // grey
			137, // purple
			13, // red
			20, // yellow
			1000, // cyan
			210, // pink
			121, // blue
			108, // grey
			2133, // purple
			232, // red
			2345, // yellow
			200, // cyan
			212, // pink
			22, // blue
			130, // grey
			2043, // purple
			232 };
		public static Integer[] penalty = {10, // red
			15, // yellow
			10, // cyan
			4, // pink
			15, // blue
			35, // grey
			300, // purple
			46, // red
			127, // yellow
			55, // cyan
			103,
			10, // red
			20, // yellow
			220, // cyan
			400, // pink
			150, // blue
			600, // grey
			250, // purple

			1100, // red
			520, // yellow
			2420, // cyan
			940, // pink
			507, // blue
			63, // grey
			521, // purple

			110, // red
			220, // yellow
			20, // cyan
			40, // pink
			504, // blue
			620, // grey
			504, // purple

			70, // red
			210, // yellow
			24, // cyan
			4000, // pink
			10, // blue
			640, // grey
			530, // purple

			100, // red
			213, // yellow
			20, // cyan
			56, // pink
			420, // blue
			64, // grey
			58, // purple

			11, // red
			20, // yellow
			210, // cyan
			44, // pink
			59, // blue
			604, // grey
			52, // purple
			1034 };

}
