
package in.ac.iiitd.spatial;

/*
 *	Description about the class 
 */
public class MinHeapNode {
	//stores the minimum distance of demandNode to particular Service center
	private int distance;
	//stores demand Node id
    private String demandNodeID;
    //stores service center id of service center from which distance of demand node is minimum
    private String serviceCenterID;
	public MinHeapNode(int distance, String demandNodeID, String serviceCenterID) {
		super();
		this.distance = distance;
		this.demandNodeID = demandNodeID;
		this.serviceCenterID = serviceCenterID;
	}
	
	public int getDistance() {
		return distance;
	}
	
	public String getDemandNodeID() {
		return demandNodeID;
	}
	
	public String getServiceCenterID() {
		return serviceCenterID;
	}
    
    

   
    
}
