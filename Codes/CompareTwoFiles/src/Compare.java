import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public class Compare {
	
	HashMap<String,String> assignment1 = new HashMap<String,String>();
	HashMap<String,String> assignment2 = new HashMap<String,String>();
	
	public void LoadFile1(String file1) throws IOException{
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file1));
		String line = "";
		while ((line = bufferedReader.readLine()) != null) {
			String[] det = line.split(";");
			if(det.length == 1){
				break;
			}
			assignment1.put(det[0], det[1]);
		}
		bufferedReader.close();
	}
	public void LoadFile2(String file2)throws IOException{
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file2));
		String line = "";
		while ((line = bufferedReader.readLine()) != null) {
			String[] det = line.split(";");
			if(det.length == 1){
				break;
			}
			assignment2.put(det[0], det[1]);
		}
		bufferedReader.close();
	}
	public int CompareAssignments(){
		int wrong = 0;
		Iterator<String> iter1 = assignment1.keySet().iterator();
		while(iter1.hasNext()){
			String DNode = iter1.next();
			if(!assignment1.get(DNode).equals(assignment2.get(DNode))){
				wrong++;
			}
		}
		return wrong;
	}
	public int getTotalCount(){
		return assignment1.size();
	}
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Compare c = new Compare();
		c.LoadFile1("file1.txt");
		c.LoadFile2("file2.txt");
		System.out.println("Correct Assignments "+(c.getTotalCount() - c.CompareAssignments()));
		System.out.println("Wrong Assignments "+c.CompareAssignments());
		
	}

}
