import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

public class Preprocessor {

	HashMap<String,Integer> allNodes;
	HashMap<String,String> serviceCenters;
	HashMap<String,Integer> centerIndex;
	int[][] costMatrix;
	String fileAllNodes; 
	String fileServiceCenters;
	String fileCostMatrix;
	Preprocessor(String fileAllNodes,String fileCostMatrix,String fileServiceCenters,int size){
		this.fileAllNodes = fileAllNodes;
		this.fileCostMatrix = fileCostMatrix;
		this.fileServiceCenters = fileServiceCenters;
		this.allNodes = new HashMap<String,Integer>();
		this.serviceCenters = new HashMap<String,String>();
		this.centerIndex = new HashMap<String,Integer>();
		this.costMatrix   = new int[size][];
		for(int i=0;i<size;i++){
			costMatrix[i] = new int[size];
		}
	}
	
	void populateAllNodeIds() throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(this.fileAllNodes));
		int counter = 0;
		while(true){
			String nodeId = reader.readLine();
			if(nodeId == null){
				break;
			}
			else{
				allNodes.put(nodeId, counter);
				counter++;
			}
		}
		reader.close();
	}
	void populateServiceCenters()throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(this.fileServiceCenters));
		int counter = 0;
		while(true){
			String centerDetails = reader.readLine();
			if(centerDetails == null){
				break;
			}
			else{
				String[] delimitedDetails = centerDetails.split(",");
				serviceCenters.put(delimitedDetails[4],delimitedDetails[7]+"-"+delimitedDetails[8]);
				counter++;
			}
		}
		reader.close();
	}
	void populateCostMatrix()throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(this.fileCostMatrix));
		for(int i=0;i<costMatrix.length;i++){
			String[] costs = reader.readLine().split(",");
			for(int j=0;j<costMatrix.length;j++){
				costMatrix[i][j] = Integer.parseInt(costs[j].replaceAll("\\s",""));
			}
		}
		reader.close();
	}
	
	private void getServiceIndexAndRemoveCenters() {
		// TODO Auto-generated method stub
		Iterator<String> it = allNodes.keySet().iterator();
		//int count = 0;
		while(it.hasNext()){
			String nodeId = it.next();
			if(serviceCenters.containsKey(nodeId)){
				//int position = serviceCenters.get(nodeId);
				System.out.println("Service Center Matched "+nodeId);
				/*if(count < noOfCenters )
				{
					
				}
				count++; */
				centerIndex.put(nodeId,allNodes.get(nodeId));
				it.remove();
				
			}
		}
		
	}
	private static int getRandomNo(int min,int max){
		Random rand = new Random();
		int randomNo = rand.nextInt((max-min)+1) + min;
		return randomNo;
	}
	public void formatToHungarian() throws IOException{
		BufferedWriter writer = new BufferedWriter(new FileWriter("input16.txt"));
		int noOfDemandNodes = allNodes.size(); 
		writer.write(noOfDemandNodes+";"+centerIndex.size());
		Iterator<String> iteratorCenters = centerIndex.keySet().iterator();
		writer.newLine();
		while(iteratorCenters.hasNext()){
			String nodeId = iteratorCenters.next();
			//int capacity = getRandomNo(2300, 2600);
			//int penalty  = getRandomNo(150,500);
			int capacity = Integer.parseInt(serviceCenters.get(nodeId).split("-")[0]);
			int penalty  = Integer.parseInt(serviceCenters.get(nodeId).split("-")[1]);
			writer.write(nodeId+";"+capacity+";"+penalty);
			writer.newLine();
					
		}
		Iterator<String> iteratorDemandNodes = allNodes.keySet().iterator();
		while(iteratorDemandNodes.hasNext()){
			String nodeId = iteratorDemandNodes.next();
			
			String ithLine = nodeId+";";
			int i = allNodes.get(nodeId);
			Iterator it = centerIndex.keySet().iterator();
			while(it.hasNext()){
				int j = centerIndex.get(it.next());
				ithLine += costMatrix[i][j]+";";
			}
			writer.write(ithLine);
			writer.newLine();
			
		}
		writer.close();
	}
	
	
	public static void main(String[] args) throws IOException  {
		Preprocessor p = new Preprocessor("allNew16.txt","16distance.txt","pureServiceVertices16.txt",16);
		p.populateAllNodeIds();
		p.populateCostMatrix();
		p.populateServiceCenters();
		p.getServiceIndexAndRemoveCenters();
		p.formatToHungarian();
	}

	
}
