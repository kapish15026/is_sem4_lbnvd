import java.io.IOException;

public class Simulator {

	public static final String CONSTFILENAME = "input";
	public static void main(String[] args) throws IOException  {
		// TODO Auto-generated method stub
	/*	double[][] cost = new double[][]{{5,5,10,10,10,9,9,24,24,24},{10,10,15,15,15,2,2,17,17,17},
						{5,5,10,10,10,3,3,18,18,18},{9,9,14,14,14,5,5,20,20,20}
						,{3,3,8,8,8,11,11,26,26,26},{100,100,100,100,100,100,100,100,100,100},
						{100,100,100,100,100,100,100,100,100,100},{100,100,100,100,100,100,100,100,100,100}
						,{100,100,100,100,100,100,100,100,100,100},{100,100,100,100,100,100,100,100,100,100}}; 
		
		//double [][]cost = new double[16000][16000];
		for(int i=0;i<16000;i++){
			
			for(int j=0;j<16000;j++){
				Random rn = new Random();
				cost[i][j] = rn.nextInt(i+j+1) + 3; 
				
			}
		} */
		String str = args[0];
		PreProcessor p = new PreProcessor(CONSTFILENAME+str+".txt");
		double[][] costMatrix= p.getCostMatrix();
		//p.printMatrix();
		//System.out.println("Got Cost Matrix Size is "+costMatrix.length);
		Hungarian a = new Hungarian(costMatrix);
		long startTime = System.nanoTime();
		int[] result= a.execute();
		long endTime = System.nanoTime();
		long duration = (endTime - startTime); 
		double minWeight = 0;
		String[] demandNodeLabels    = p.getDemandNodeLabels();
		String[] serviceCenterLabels = p.getServiceCenterLabels();
		int totalDemandNodes = p.getTotalNoOfDemandNodes();
		for(int i=0;i<result.length;i++){
			//System.out.println("Demand Node "+i+" is assigned Service center: "+result[i]);
			if(i < totalDemandNodes){
				
				int index = result[i]/totalDemandNodes;
				System.out.println(demandNodeLabels[i]+";"+serviceCenterLabels[index]);
			}
			minWeight += costMatrix[i][result[i]];
		}
		minWeight -= p.getDummyNodes() * p.getDummyWeight();
		System.out.println(minWeight);
		System.out.println(duration/1000000000); //in seconds
	}

}
